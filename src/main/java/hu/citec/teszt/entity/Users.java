package hu.citec.teszt.entity;

public class Users {

	private String fullname;
	private String name;
	private String password;
	private Integer id;

	public Users() {
	}

	public Users(String fullname, String name, String password, Integer id) {
		this.fullname = fullname;
		this.name = name;
		this.password = password;
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Users [fullname=" + fullname + ", name=" + name + ", password=" + password + ", id=" + id + "]";
	}
}
