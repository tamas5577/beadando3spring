package hu.citec.teszt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.citec.teszt.dao.LoginRepository;
import hu.citec.teszt.dao.ProductRepository;
import hu.citec.teszt.entity.Product;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private LoginRepository loginRepository;

	public ProductService(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public List<Product> findProducts() {
		return productRepository.findProducts();
	}

	public Product findProductyById(int productId) {
		return productRepository.findProductyById(productId);
	}

	public void addProduct(Product product) {
		productRepository.addProduct(product);
	}

	public void editProduct(Product product) {
		productRepository.editProduct(product);
	}

	public void deleteProductByID(int productId) {
		productRepository.deleteProductById(productId);
	}
}
