package hu.citec.teszt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hu.citec.teszt.dao.LoginRepository;
import hu.citec.teszt.entity.Users;

@Service
public class LoginService {

	@Autowired
	private LoginRepository loginRepo;

	public LoginService(LoginRepository loginRepository) {
		this.loginRepo = loginRepository;
	}

	public boolean validateUser(String username, String password) {
		
		Users u = loginRepo.validateUser(username);
		//System.out.println("u.getname:" + u.getName());
		
		if (u.getPassword().equals(password)) {
			//System.out.println("Sikeres bejelentkezés!");
			return true;
		} else {
			//System.out.println("Rossz a felhasználónév vagy a jelszó!");
			return false;
		}
	}
}
