package hu.citec.teszt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.citec.teszt.service.LoginService;
import hu.citec.teszt.service.ProductService;

@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@Autowired
	private HttpServletRequest request;

	@GetMapping("/login")
	public String login() {
		return "login";
	}

	@PostMapping("/login")
	public String doLogin(@RequestParam String user_name, @RequestParam String user_password, Model model) {
		
		// System.out.println("user_name: " + user_name + ", user_password: " + user_password);
		
		if(loginService.validateUser(user_name, user_password)) {
			HttpSession session = request.getSession(true);
			
			session.setAttribute("user", user_name);
			session.setMaxInactiveInterval(60);
			return "redirect:/products/";
		}else {
			model.addAttribute("wrong", "error");
			return "login";
		}
		
//		if (user_name.equals("admin") && user_password.equals("admin")) {
//			HttpSession session = request.getSession(true);
//			
//			session.setAttribute("user", user_name);
//			session.setMaxInactiveInterval(60);
//			return "redirect:/products/";
//		} else {
//			model.addAttribute("wrong", "error");
//			return "login";
//		}
	}
}
