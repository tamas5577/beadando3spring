package hu.citec.teszt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import hu.citec.teszt.entity.Product;
import hu.citec.teszt.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private HttpServletRequest request;

	@GetMapping("/")
	public String products(Model model) {
		HttpSession session = request.getSession(false);

		//System.out.println("session: " + session);
		
		if (session == null || session.getAttribute("user") == null) {
			return "login";
		}

		model.addAttribute("products", productService.findProducts());

		return "product/products";
	}

	@GetMapping("/edit/{productId}")
	public String productEditor(@PathVariable int productId, Model model) {
		model.addAttribute("product", productService.findProductyById(productId));

		return "product/product-edit";
	}

	@PostMapping("/edit")
	public String editProduct(@ModelAttribute Product product) {
		productService.editProduct(product);

		return "redirect:/products/";
	}

	@GetMapping("/new")
	public String productCreator(Model model) {
		model.addAttribute("product", new Product());

		return "product/product-create";
	}

	@PostMapping("/new")
	public String createProduct(@ModelAttribute Product product) {
		productService.addProduct(product);

		return "redirect:/products/";
	}

	@GetMapping("/delete/{productId}")
	public String deleteProduct(@PathVariable int productId) {
		productService.deleteProductByID(productId);
		return "redirect:/products/";
	}

	@GetMapping("/logout")
	public String logoutSession() {
		HttpSession session = request.getSession(false);
		session.invalidate();
		return "redirect:/login";
	}
}
