package hu.citec.teszt.dao;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import hu.citec.teszt.entity.Product;
import hu.citec.teszt.entity.Users;

@Repository
public class ProductRepository {

	private JdbcTemplate jdbcTemplate;

	public ProductRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Product> findProducts() {
		String query = "SELECT id, name, type, price FROM product";
		return jdbcTemplate.query(query, BeanPropertyRowMapper.newInstance(Product.class));
	}

	public Product findProductyById(int productId) {
		String query = "SELECT id, name, type, price FROM product WHERE id = ?";
		return jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(Product.class), productId);
	}

	public void addProduct(Product product) {
		String query = "INSERT INTO product (name, type, price) VALUES (?, ?, ?)";
		jdbcTemplate.update(query, product.getName(), product.getType(), product.getPrice());
	}

	public void editProduct(Product product) {
		String query = "UPDATE product SET name = ?, type = ?, price = ? WHERE id = ?";
		jdbcTemplate.update(query, product.getName(), product.getType(), product.getPrice(), product.getId());
	}

	public boolean deleteProductById(int productId) {
		String query = "delete from product where id =" + productId;
		jdbcTemplate.execute(query);
		return true;
	}
}
