package hu.citec.teszt.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import hu.citec.teszt.entity.Users;

@Repository
public class LoginRepository {

	private JdbcTemplate jdbcTemplate;

	public LoginRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public Users validateUser(String username) {
		String query = "SELECT full_name as fullname, username as name, user_password as password, user_id as id FROM users WHERE username=?";
		Users u = jdbcTemplate.queryForObject(query, BeanPropertyRowMapper.newInstance(Users.class), username);
		return u;
	}
}